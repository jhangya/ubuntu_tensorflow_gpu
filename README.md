# Install  
<pre>
軟體需求
你的系統上必須安裝下列 NVIDIA® 軟體：
    NVIDIA® GPU 驅動程式：CUDA® 10.1 需要 418.x 或較新版本。
    CUDA® Toolkit：TensorFlow 支援 CUDA® 10.1 (TensorFlow 2.1.0 以上版本)
    CUDA® Toolkit 隨附 CUPTI。
    cuDNN SDK 7.6
    (選用) TensorRT 6.0 可改善某些模型的推論延遲情況和總處理量。
</pre>

cuda toolkit 與 driver 需要一致 (最好完全相同)，為了符合 tensorflow cuda 版本要求，須安裝 cuda 10.1

|CUDA Toolkit           | Linux x86_64 Driver Version  
------------------------|:-----------------------------:
 CUDA 11.1 (11.1.0)     | >= 450.80.02                 
 CUDA 11.0 (11.0.3)     | >= 450.36.06                 
 CUDA 10.2 (10.2.89)    | >= 440.33                    
 CUDA 10.1 (10.1.105)   | >= 418.39                    
 CUDA 10.0 (10.0.130)   | >= 410.48                    
 CUDA 9.2 (9.2.88)      | >= 396.26                    
 CUDA 9.1 (9.1.85)      | >= 390.46                    
 CUDA 9.0 (9.0.76)      | >= 384.81                    
 CUDA 8.0 (8.0.61 GA2)  | >= 375.26                    
 CUDA 8.0 (8.0.44)      | >= 367.48                    
 CUDA 7.5 (7.5.16)      | >= 352.31                    
 CUDA 7.0 (7.0.28)      | >= 346.46                    

* Step0. 卸載舊版 
<pre>
# sudo rm /etc/apt/sources.list.d/cuda*
# sudo apt-get --purge remove "*cublas*" "cuda*" "nsight*" 
# sudo apt-get --purge remove "*nvidia*"
# sudo apt-get autoremove
# sudo apt-get autoclean
# sudo rm -rf /usr/local/cuda*
</pre>

* Step1. 安裝 nvidia driver
安裝 nvidia-driver-418-server，之後重開機，執行 nvidia-smi

<pre>
sudo add-apt-repository ppa:graphics-drivers  #添加NVIDA显卡驱动库
sudo apt update
ubuntu-drivers devices  #显示可安装驱动

== /sys/devices/pci0000:00/0000:00:01.0/0000:01:00.0 ==
modalias : pci:v000010DEd00001EB1sv000010DEsd000012A0bc03sc00i00
vendor   : NVIDIA Corporation
model    : TU104GL [Quadro RTX 4000]
driver   : nvidia-driver-455 - third-party free recommended
driver   : nvidia-driver-440-server - distro non-free
driver   : nvidia-driver-450-server - distro non-free
driver   : nvidia-driver-418-server - distro non-free
driver   : nvidia-driver-450 - distro non-free
driver   : xserver-xorg-video-nouveau - distro free builtin

</pre>

* Step2. 安裝 cuda toolkit
<pre>
sudo bash cuda_10.1.243_418.87.00_linux.run --toolkit --samples --installpath=/home/oujy/tools/cuda_10.1 --override
</pre>

* Step3. 安裝 cudnn
<pre>
tar -xzvf cudnn-10.1-linux-x64-v7.6.5.32.tgz
sudo cp cuda/include/cudnn.h ~/tools/cuda-toolkit_10.1/include/
sudo cp cuda/lib64/libcudnn* ~/tools/cuda-toolkit_10.1/lib64/
</pre>


* Step3. 安裝 TensorRT6
<pre>
tar -zxvf TensorRT-6.0.1.5.Ubuntu-18.04.x86_64-gnu.cuda-10.1.cudnn7.6.tar.gz
</pre>

* Step4. 設定環境變數
<pre>
export PATH=${PATH}:/home/oujy/tools/cuda-toolkit_10.1/bin
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/oujy/tools/cuda-toolkit_10.1/lib64:/home/oujy/tools/cuda-toolkit_10.1/targets/x86_64-linux/lib:/home/oujy/tools/cuda-toolkit_10.1/targets/x86_64-linux/include
export PATH=${PATH}:/home/oujy/tools/TensorRT-6.0.1.5/bin/
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/oujy/tools/TensorRT-6.0.1.5/lib/:/home/oujy/tools/TensorRT-6.0.1.5/include/
</pre>

* Step5. 安裝 nvidia-modprobe
nvidia-modprobe - Load the NVIDIA kernel module and create NVIDIA character device files.
<pre>
sudo apt install nvidia-modprobe
</pre>

* Step6. 安裝 tensorflow
** 安裝 anaconda
** 安裝 tensorflow
<pre>
#進入 anaconda
source ${anaconda_dir}/bin/activate
pip install  tensorflow

# 測試是否有抓到 gpu
python
import tensorflow as tf
tf.config.list_physical_devices('GPU')

#離開 anaconda
source ${anaconda_dir}/bin/deactivate
</pre>

<pre>
python
import tensorflow as tf
tf.config.list_physical_devices('GPU')
</pre>

